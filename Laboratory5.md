# EXERCISE 2

## GitHub VS Gitlab

**Which one would you like to try?**

I would try to prove GitLab because I like the interface, I think is very friendly.

## Differences between GitHub and GitLab

**_First difference:_**

In GitLab you have more submenus that are more accessible than in GitHub because you have it on one side of the screen. In github you need to click in your profile for show it.

GITLAB
https://gitlab.com/Anacs55/github-vs-gitlab/-/blob/main/gitlab1.png?ref_type=heads

GITHUB
https://gitlab.com/Anacs55/github-vs-gitlab/-/blob/main/github1.png?ref_type=heads


_**Second difference:**_

I think is very usefull have an editor for create code like VSC because, in my case I use it a lot, so is more familiar for me. 
In GitHub is similar a note block.

GITLAB
https://gitlab.com/Anacs55/github-vs-gitlab/-/blob/main/GitLAB2.png?ref_type=heads

GITHUB
https://gitlab.com/Anacs55/github-vs-gitlab/-/blob/main/GITHUB2.png?ref_type=heads


**_Third difference:_**

I like can connect with others service for keep code and in GitLab you import projects from other apps like github, bickbucket or others. I think is more easy connect in GitLab than in github.

GITLAB
https://gitlab.com/Anacs55/github-vs-gitlab/-/blob/main/gitlab3.png?ref_type=heads


_**Fourth difference:**_

The news and interesting topics section in GitLab it is very extensive with many categories  instead in gitHub ypu only can search others repositorys.

GITLAB
https://gitlab.com/Anacs55/github-vs-gitlab/-/blob/main/gitlab4.PNG

GITHUB
https://gitlab.com/Anacs55/github-vs-gitlab/-/blob/main/github4.PNG


# EXERCISE 3

I didn't know that on GitHub you have unlimited private and public repositories and only paid if you want more advanced security, I thought there was a limited number. 
Other thing than i didn't know is you can upload a github in the phon, i thought than was only for desktop.
